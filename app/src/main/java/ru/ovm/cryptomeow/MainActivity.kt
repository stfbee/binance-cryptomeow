package ru.ovm.cryptomeow

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import ru.ovm.cryptomeow.charts.ChartsFragment
import ru.ovm.cryptomeow.profile.ProfileFragment

class MainActivity : AppCompatActivity() {

    private val chartsFragment: ChartsFragment = ChartsFragment.newInstance()
    private val profileFragment: ProfileFragment = ProfileFragment.newInstance()

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_chart -> {
                supportFragmentManager.beginTransaction().replace(R.id.fragment_container, chartsFragment).commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                supportFragmentManager.beginTransaction().replace(R.id.fragment_container, profileFragment).commit()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, chartsFragment).commit()
    }
}
