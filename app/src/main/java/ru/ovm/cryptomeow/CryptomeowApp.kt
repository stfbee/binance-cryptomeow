package ru.ovm.cryptomeow

import android.app.Application
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.ovm.cryptomeow.api.BinanceApi
import ru.ovm.cryptomeow.api.security.AuthenticationInterceptor
import java.util.concurrent.TimeUnit


class CryptomeowApp : Application() {

    lateinit var binanceApi: BinanceApi

    private val secret = "CPtMIr2lnjSpsuVKAFGQaJRqWsKYSZFLTmL8AIVG8hUmPddWF5sumkXJZ6aC83b6"
    private val api = "qz2OLnXpZPao9BmaKVZzEI9S7BJCNl3h4g0BOE99jiObjuEWdUSCiwHGKfi4lcfu"

    override fun onCreate() {
        super.onCreate()

        val sharedClient = OkHttpClient.Builder()
            .pingInterval(20, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.binance.com")
            .addConverterFactory(GsonConverterFactory.create())
            .client(
                sharedClient.newBuilder()
                    .addInterceptor(AuthenticationInterceptor(api, secret))
                    .build()
            )
            .build()

        binanceApi = retrofit.create(BinanceApi::class.java)
    }


}