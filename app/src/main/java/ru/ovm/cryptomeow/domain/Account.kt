package ru.ovm.cryptomeow.domain

data class Account(
    val balances: List<AssetBalance>
)