package ru.ovm.cryptomeow.domain

data class ChartItem(
    val symbol: String,
    val price: Float
)