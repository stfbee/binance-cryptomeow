package ru.ovm.cryptomeow.domain

data class AssetBalance(
    val asset: String,
    val free: Float,
    val locked: Float
)