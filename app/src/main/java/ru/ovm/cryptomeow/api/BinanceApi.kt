package ru.ovm.cryptomeow.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query
import ru.ovm.cryptomeow.domain.Account
import ru.ovm.cryptomeow.domain.ChartItem

interface BinanceApi {
    @GET("/api/v3/ticker/price")
    fun getPrices(): Call<List<ChartItem>>

    @Headers("SIGNED: #")
    @GET("/api/v3/account")
    fun getAccount(
        @Query("recvWindow") recvWindow: Long,
        @Query("timestamp") timestamp: Long
    ): Call<Account>
}