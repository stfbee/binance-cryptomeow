package ru.ovm.cryptomeow.api.security

import okhttp3.Interceptor
import okhttp3.Response

/**
 * A request interceptor that injects the API Key Header into requests, and signs messages, whenever required.
 *
 * https://github.com/binance-exchange/binance-java-api/ (edited)
 */
class AuthenticationInterceptor(private val apiKey: String, private val secret: String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {

        val original = chain.request()
        val newRequestBuilder = original.newBuilder()

        val isSignatureRequired = original.header("SIGNED") != null
        newRequestBuilder.removeHeader("SIGNED")

        // Endpoint requires sending a valid API-KEY
        if (isSignatureRequired) {
            newRequestBuilder.addHeader("X-MBX-APIKEY", apiKey)

            val payload = original.url().query()
            if (payload!!.isNotEmpty()) {
                val signature = HmacSHA256Signer.sign(payload, secret)
                val signedUrl = original.url().newBuilder().addQueryParameter("signature", signature).build()
                newRequestBuilder.url(signedUrl)
            }
        }

        // Build new request after adding the necessary authentication information
        val newRequest = newRequestBuilder.build()
        return chain.proceed(newRequest)

    }
}