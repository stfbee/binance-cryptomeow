package ru.ovm.cryptomeow.charts

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.chart_list_view.view.*
import ru.ovm.cryptomeow.domain.ChartItem

class ChartHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(item: ChartItem) = with(itemView) {
        symbol.text = item.symbol
        price.text = item.price.toString()
    }
}