package ru.ovm.cryptomeow.charts

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import ru.ovm.cryptomeow.R
import ru.ovm.cryptomeow.domain.ChartItem

class ChartAdapter(private val items: List<ChartItem>) : RecyclerView.Adapter<ChartHolder>() {

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ChartHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChartHolder {
        return ChartHolder(LayoutInflater.from(parent.context).inflate(R.layout.chart_list_view, parent, false))
    }
}

