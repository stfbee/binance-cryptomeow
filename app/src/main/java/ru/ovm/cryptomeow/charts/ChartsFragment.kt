package ru.ovm.cryptomeow.charts

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_charts.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.ovm.cryptomeow.CryptomeowApp
import ru.ovm.cryptomeow.R
import ru.ovm.cryptomeow.domain.ChartItem
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.concurrent.timerTask

class ChartsFragment : Fragment() {
    private lateinit var timer: Timer
    private lateinit var adapter: ChartAdapter

    private var items = ArrayList<ChartItem>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.fragment_charts, container, false)
    }

    override fun onResume() {
        super.onResume()

        timer = Timer()
        timer.scheduleAtFixedRate(
            timerTask { hiddenUpdate() },
            TimeUnit.SECONDS.toMillis(3),
            TimeUnit.SECONDS.toMillis(3)
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = ChartAdapter(items)
        recycler_view.adapter = adapter

        retry_load.setOnClickListener {
            loadItems()
        }

        loadItems()
    }

    private fun hiddenUpdate() {
        (activity?.application as CryptomeowApp).binanceApi.getPrices()
            .enqueue(object : Callback<List<ChartItem>?> {
                override fun onFailure(call: Call<List<ChartItem>?>, t: Throwable) {

                    if (!isAdded) return

                    Log.e(TAG, "Error", t)
                }

                override fun onResponse(call: Call<List<ChartItem>?>, response: Response<List<ChartItem>?>) {

                    if (!isAdded) return

                    Log.i(TAG, "Prices response $response")
                    showItems(response.body())
                }
            })
    }

    private fun loadItems() {
        showLoader()

        (activity?.application as CryptomeowApp).binanceApi.getPrices()
            .enqueue(object : Callback<List<ChartItem>?> {
                override fun onFailure(call: Call<List<ChartItem>?>, t: Throwable) {

                    if (!isAdded) return

                    Log.e(TAG, "Error", t)
                    showError()
                }

                override fun onResponse(call: Call<List<ChartItem>?>, response: Response<List<ChartItem>?>) {

                    if (!isAdded) return

                    Log.i(TAG, "Prices response $response")
                    showItems(response.body())
                }
            })
    }

    private fun showError() {
        error_group.visibility = View.VISIBLE
        recycler_view.visibility = View.GONE
        progress_circular.visibility = View.GONE
    }

    private fun showLoader() {
        recycler_view.visibility = View.GONE
        error_group.visibility = View.GONE
        progress_circular.visibility = View.VISIBLE
    }

    private fun showItems(newItems: List<ChartItem>?) {

        newItems ?: let {
            showError()
            return
        }
        items.clear()
        items.addAll(newItems)

        recycler_view.visibility = View.VISIBLE
        error_group.visibility = View.GONE
        progress_circular.visibility = View.GONE

        adapter.notifyDataSetChanged()
    }

    override fun onPause() {
        super.onPause()

        timer.cancel()
    }

    companion object {
        val TAG: String = ChartsFragment::class.java.simpleName

        fun newInstance(): ChartsFragment {
            return ChartsFragment()
        }
    }
}