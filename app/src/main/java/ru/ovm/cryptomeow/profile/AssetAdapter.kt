package ru.ovm.cryptomeow.profile

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import ru.ovm.cryptomeow.R
import ru.ovm.cryptomeow.domain.AssetBalance

class AssetAdapter(private val items: List<AssetBalance>) : RecyclerView.Adapter<AssetHolder>() {

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: AssetHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AssetHolder {
        return AssetHolder(LayoutInflater.from(parent.context).inflate(R.layout.asset_list_view, parent, false))
    }
}

