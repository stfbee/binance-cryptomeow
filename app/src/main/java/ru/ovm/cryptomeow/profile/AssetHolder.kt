package ru.ovm.cryptomeow.profile

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.asset_list_view.view.*
import ru.ovm.cryptomeow.domain.AssetBalance

class AssetHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(item: AssetBalance) = with(itemView) {
        symbol.text = item.asset
        free_value.text = item.free.toString()
        locked_value.text = item.locked.toString()
    }
}