package ru.ovm.cryptomeow.profile

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_profile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.ovm.cryptomeow.CryptomeowApp
import ru.ovm.cryptomeow.R
import ru.ovm.cryptomeow.charts.ChartsFragment
import ru.ovm.cryptomeow.domain.Account
import ru.ovm.cryptomeow.domain.AssetBalance
import java.util.*
import java.util.concurrent.TimeUnit

class ProfileFragment : Fragment() {
    private var items = ArrayList<AssetBalance>()
    private lateinit var adapter: AssetAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = AssetAdapter(items)
        recycler_view.adapter = adapter

        retry_load.setOnClickListener {
            loadProfile()
        }

        loadProfile()
    }

    private fun loadProfile() {
        showLoader()

        (activity?.application as CryptomeowApp).binanceApi.getAccount(
            TimeUnit.MINUTES.toMillis(1),
            System.currentTimeMillis()
        ).enqueue(object : Callback<Account?> {
            override fun onFailure(call: Call<Account?>, t: Throwable) {

                if (!isAdded) return

                Log.e(ChartsFragment.TAG, "Error", t)
                showError()
            }

            override fun onResponse(call: Call<Account?>, response: Response<Account?>) {

                if (!isAdded) return

                val body = response.body()

                Log.d(ChartsFragment.TAG, "Account response: $body")

                if (body?.balances?.isEmpty() == true) {
                    showEmpty()
                } else {
                    showBalances(body?.balances)
                }
            }
        })
    }

    private fun showBalances(balances: List<AssetBalance>?) {
        balances ?: let {
            showError()
            return
        }

        items.clear()
        items.addAll(balances)

        recycler_view.visibility = View.VISIBLE
        error_group.visibility = View.GONE
        progress_circular.visibility = View.GONE

        adapter.notifyDataSetChanged()
    }

    private fun showLoader() {
        recycler_view.visibility = View.GONE
        error_group.visibility = View.GONE
        progress_circular.visibility = View.VISIBLE
    }

    private fun showError() {
        error_group.visibility = View.VISIBLE
        recycler_view.visibility = View.GONE
        progress_circular.visibility = View.GONE

        error_text.setText(R.string.load_error)
        error_image.setImageResource(R.drawable.no_signal_grayscale)
    }

    private fun showEmpty() {
        error_group.visibility = View.VISIBLE
        recycler_view.visibility = View.GONE
        progress_circular.visibility = View.GONE

        error_text.setText(R.string.load_empty)
        error_image.setImageResource(R.drawable.no_items_greyscale)
    }

    companion object {
        fun newInstance(): ProfileFragment {
            return ProfileFragment()
        }
    }
}